<?php

header('Content-type: application/json; charset=utf-8');

$livelinks = require(kirby()->roots()->snippets() . DS . 'livelinks.php');
$projects = require(kirby()->roots()->snippets() . DS . 'projects.php');
$pages = require(kirby()->roots()->snippets() . DS . 'pages.php');
$about = require(kirby()->roots()->snippets() . DS . 'about.php');


echo json_encode(array(
  'links' => $livelinks,
  'projects' => $projects,
  'pages' => $pages,
  'about' => $about,
));

?>
