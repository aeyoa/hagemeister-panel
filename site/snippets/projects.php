<?php

$data = $pages->find('projects')->children()->visible();

$projects = array();

foreach($data as $project) {

  if ($project->hasImages()) {

    $images = array();

    foreach($project->images() as $image) {
      $images[] = array(
        'url' => $image->url(),
        'thumb' => $image->thumb(['height'  => 160, 'quality' => 70])->url(),
        'title' => (string)$image->title(),
        'subtitle' => (string)$image->subtitle(),
      );
    }

    $projects[] = array(
      'thumb' => $project->images()->first()->thumb(['width'  => 550, 'height'  => 550, 'quality' => 70])->url(),
      'slug' => (string)$project->slug('en'),
      'title' => (string)$project->title(),
      'description' => (string)$project->text()->kt(),
      'tags' => (string)$project->tags(),
      'images' => $images
    );

  }

}

return $projects;

?>
