<?php

$page = $pages->find('livelinks');

return array(
  array(
    'id' => 'fools-book1',
    'title' => (string)$page->foolsbook1title(),
    'text' => (string)$page->foolsbook1text(),
    'link' => (string)$page->foolsbook1link(),
  ),
  array(
    'id' => 'fools-book2',
    'title' => (string)$page->foolsbook2title(),
    'text' => (string)$page->foolsbook2text(),
    'link' => (string)$page->foolsbook2link(),
  ),
  array(
    'id' => 'fools-sasha',
    'title' => (string)$page->foolssashatitle(),
    'text' => (string)$page->foolssashatext(),
    'link' => (string)$page->foolssashalink(),
  ),
  array(
    'id' => 'fools-book3',
    'title' => (string)$page->foolsbook3title(),
    'text' => (string)$page->foolsbook3text(),
    'link' => (string)$page->foolsbook3link(),
  ),
  array(
    'id' => 'fools-book4',
    'title' => (string)$page->foolsbook4title(),
    'text' => (string)$page->foolsbook4text(),
    'link' => (string)$page->foolsbook4link(),
  ),
  array(
    'id' => 'fontanka-smotr',
    'title' => (string)$page->fontankasmotrtitle(),
    'text' => (string)$page->fontankasmotrtext(),
    'link' => (string)$page->fontankasmotrlink(),
  ),
  array(
    'id' => 'atlantide-tower',
    'title' => (string)$page->atlantidetowertitle(),
    'text' => (string)$page->atlantidetowertext(),
    'link' => (string)$page->atlantidetowerlink(),
  ),
  array(
    'id' => 'atlantide-bridge',
    'title' => (string)$page->atlantidebridgetitle(),
    'text' => (string)$page->atlantidebridgetext(),
    'link' => (string)$page->atlantidebridgelink(),
  ),
  array(
    'id' => 'atlantide-сhirch',
    'title' => (string)$page->atlantidechirchtitle(),
    'text' => (string)$page->atlantidechirchtext(),
    'link' => (string)$page->atlantidechirchlink(),
  ),
  array(
    'id' => 'atlantide-couple',
    'title' => (string)$page->atlantidecoupletitle(),
    'text' => (string)$page->atlantidecoupletext(),
    'link' => (string)$page->atlantidecouplelink(),
  ),
  array(
    'id' => 'atlantide-noah',
    'title' => (string)$page->atlantidenoahtitle(),
    'text' => (string)$page->atlantidenoahtext(),
    'link' => (string)$page->atlantidenoahlink(),
  ),
  array(
    'id' => 'entry-isaacs',
    'title' => (string)$page->entryisaacstitle(),
    'text' => (string)$page->entryisaacstext(),
    'link' => (string)$page->entryisaacslink(),
  ),
  array(
    'id' => 'entry-freud',
    'title' => (string)$page->entryfreudtitle(),
    'text' => (string)$page->entryfreudtext(),
    'link' => (string)$page->entryfreudlink(),
  ),
  array(
    'id' => 'entry-jesus',
    'title' => (string)$page->entryjesustitle(),
    'text' => (string)$page->entryjesustext(),
    'link' => (string)$page->entryjesuslink(),
  ),
  array(
    'id' => 'entry-lazarus',
    'title' => (string)$page->entrylazarustitle(),
    'text' => (string)$page->entrylazarustext(),
    'link' => (string)$page->entrylazaruslink(),
  ),
  array(
    'id' => 'entry-bird',
    'title' => (string)$page->entrybirdtitle(),
    'text' => (string)$page->entrybirdtext(),
    'link' => (string)$page->entrybirdlink(),
  ),
);

?>
