<?php

$page = $site->page('about');

return array(
  'title' => (string)$page->title(),
  'intro1' => (string)$page->intro1(),
  'intro2' => (string)$page->intro2(),
  'intro3' => (string)$page->intro3(),
  'intro4' => (string)$page->intro4(),
  'intro5' => (string)$page->intro5(),
);

?>
