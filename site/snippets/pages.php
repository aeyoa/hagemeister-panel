<?php

$data = $pages->find('pages')->children();

$pages = array();

foreach($data as $page) {

  $pages[] = array(
    'slug' => (string)$page->slug('en'),
    'title' => (string)$page->title(),
    'text' => (string)$page->text()->kt(),
  );

}

return $pages;

?>
